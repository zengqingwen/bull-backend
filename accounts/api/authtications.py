from rest_framework import exceptions
from rest_framework.authtoken.models import Token
from rest_framework.authentication import BaseAuthentication, SessionAuthentication


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

class FirstAuthtication(BaseAuthentication):
    def authenticate(self,request):
        pass

    def authenticate_header(self, request):
        pass

class Authtication(BaseAuthentication):
    def authenticate(self,request):
        # user = request.user ???
        print("#####")
        token = request._request.META.get('HTTP_AUTHORIZATION')
        # print(request._request.META.get('HTTP_AUTHORIZATION'))
        # print(request.META.get("HTTP_AUTHORIZATION"))
        token = token.split()[1]
        print(token)
        print("token")
        token_obj = Token.objects.filter(key=token).first()
        if not token_obj:
            raise exceptions.AuthenticationFailed('用户认证失败')
        # 在rest framework内部会将整个两个字段赋值给request，以供后续操作使用
        return (token_obj.user, token_obj)

    def authenticate_header(self, request):
        return 'Basic realm="api"'