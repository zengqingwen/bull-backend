from django.contrib import admin
from .models import UserProfile

# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'bio', 'avatar', 'status', 'name')
    search_fields = ('name',)
    list_filter = ('status', 'name',)

admin.site.register(UserProfile, UserProfileAdmin)