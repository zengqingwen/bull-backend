from rest_framework.views import APIView
from rest_framework.response import Response

# https://www.jianshu.com/p/860859b7dea5 postman向Django-restframework 提交三种数据类型总结
from rest_framework.parsers import JSONParser

from ...utils.assem import init_or_fetch_git, init_or_fetch_branchtag

class InitOrFetchBranchTagAPIView(APIView):
    def get(self, request, *args, **kwargs):
        ret = {'code': 1000, 'msg': None, 'data': None}
        ret = init_or_fetch_branchtag(request, ret)
        if ret['code'] != 1000:
            return Response(ret)

        return Response(ret)

class InitOrFetchRepoAPIView(APIView):
    parser_classes = [JSONParser, ]
    def post(self, request, *args, **kwargs):
        ret = {'code': 1000, 'msg': None, 'data': None}
        # print("11111111")
        # print(request)
        # print(request._request)
        # print(request._request.body)
        # print(request._request.POST)
        # print(request.data)
        ret = init_or_fetch_git(request, ret)
        # if ret['code'] != 1000:
        #     return Response(ret)

        return Response(ret)
        # courses = Course.objects.all()
        # serializer = CourseSerializer(courses, many=True)
        # return Response(serializer.data)
