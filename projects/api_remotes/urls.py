from django.conf.urls import url
from django.urls import include, path
from django.contrib import admin

from .gitlab_git_category.views import InitOrFetchRepoAPIView, InitOrFetchBranchTagAPIView

urlpatterns = [
    path('gitlab-git/init-or-fetch-commits/', InitOrFetchRepoAPIView.as_view(), name='init-or-fetch-commits-repo'),
    path('gitlab-git/init-or-fetch-branchestags/', InitOrFetchBranchTagAPIView.as_view(), name='init-or-fetch-branchestags'),
]