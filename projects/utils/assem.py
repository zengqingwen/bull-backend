import os
import json
from django.conf import settings

from ..models import Credential
from ..utils.gitlab_handler import GitlabAPI

from django.utils.translation import ugettext_lazy as _
import rest_framework
import git


from .common import (
    hashlize_branchtag,
    localize_date,
    handle_point,
    transform_filter_parmes,
    transform_display_columns,
    format_commit_log,
    makeDir,
    getRepoAndUpdateRemoteOrigin,
)

import yaml

"""
belows are assemble common_used functions
"""
def init_or_fetch_branchtag(request, ret):
    req_params = request.query_params.dict()
    # username = request.user.username
    # projectname = req_params.pop('Name')
    # this_workspace = os.path.join(settings.PROJECT_HOME_DIR, '.bull', username, projectname)
    # repo = git.Repo(this_workspace)
    # branchestags = repo.refs
    # refs_remotes_origin = [i.remote_head for i in branchestags if i.is_remote()]
    # refs_heads = []
    # refs_tags = [i.name for i in repo.refs if i.object.type == 'tag']
    # ret['msg'] = {"branches": refs_remotes_origin, "tags": refs_tags}


    # credential_id = request.META.get('HTTP_CREDENTIAL_ID')
    # origin_url = request.META.get('HTTP_ORIGIN_URL')
    # projectid = request.META.get('HTTP_ORIGIN_FORTH')

    credential_id = req_params.pop('Credential-Id')
    origin_url = req_params.pop('Origin-Url')
    projectid = req_params.pop('Origin-Forth')

    gitlab_token = Credential.objects.get(id=credential_id).token
    gl = GitlabAPI(origin_url, gitlab_token)
    try:
        project_info = gl.get_project(projectid)
        branches = gl.get_project_branches(projectid)
        tags = gl.get_project_tags(projectid)
    except:
        msg = _("get the branches and tags faled")
        raise rest_framework.exceptions.NotFound(msg)
    default_branch = project_info.default_branch
    branches = [branch.name for branch in branches]
    tags = [tag.name for tag in tags]
    ret['data'] = {"branches": branches, "tags": tags, "default_branch": default_branch}
    return ret



def init_or_fetch_git(request, ret):
    req_params = request.query_params.dict()
    print(req_params)
    username = request.user.username

    """
    # https://www.cnblogs.com/cdwp8/p/5157377.html  请求头的保留字
    # this请求头

    Authorization       Token 8454491be531243e3024ecbd724a2907516c5578
    Credential-Id       23
    Origin-Url          http://192.168.56.71/
    Origin-Third        repo_test
    Origin-Jack-Name    test1
    Origin-Forth        null

    """
    # # print(request.META)
    # projectname = request.META.get('NAME')
    # credential_id = request.META.get('HTTP_CREDENTIAL_ID')
    # origin_url = request.META.get('HTTP_ORIGIN_URL')
    # origin_third = request.META.get('HTTP_ORIGIN_THIRD')
    # origin_jack_name = request.META.get('HTTP_ORIGIN_JACK_NAME')
    # origin_forth = request.META.get('HTTP_ORIGIN_FORTH')

    projectname = req_params.pop('Name')
    credential_id = req_params.pop('Credential-Id')
    origin_url = req_params.pop('Origin-Url')
    origin_third = req_params.pop('Origin-Third')
    origin_jack_name = req_params.pop('Origin-Jack-Name')
    origin_forth = req_params.pop('Origin-Forth')

    # make repo's dir
    this_workspace = os.path.join(settings.PROJECT_HOME_DIR, '.bull', username, projectname)
    print(this_workspace)
    makeDir(this_workspace)

    gitlab_token = Credential.objects.get(id=credential_id).token
    gl = GitlabAPI(origin_url, gitlab_token)
    try:
        this_project = gl.obtain_project(origin_forth) # *****比以前更进一步
    except:
        raise rest_framework.exceptions.NotFound()
    # 获取ｒｅｐｏ，并更新其remote origin 中的认证信息
    repo = getRepoAndUpdateRemoteOrigin(this_workspace, gl, gitlab_token, origin_forth)
    this_remote = repo.remotes.origin

    # 更新本地仓库
    ### 从以上gitlab中将commit-hash/branch/tag全部转换成commit-hash
    # 然后ｇｉｔ　ｆｅｔｃｈ（）本地所有内容　ｔａｇｓ、ｏｒｉｇｉｎ/ｂｒａｎｃｈｅｓ
    try:
        this_remote.fetch()  # 该条命令更新了远程的所有内容包括ｔａｇ，所以不用再this_remote.fetch("--tags")
        # this_remote.fetch("--tags")
    except:
        print("The repo does not update, yet!")

    # 整理ｐｏｓｔ过来的参数
    form_params = request.data
    print(form_params)
    hashlize_branchtag(form_params, this_project)
    localize_date(form_params)
    handle_point(form_params)

    filter_parms = transform_filter_parmes(form_params)
    display_parms, front_column_dispaly = transform_display_columns(form_params)
    # filter_parms.append(display_parms)
    filter_parms.insert(0, display_parms)
    git_cmd = filter_parms
    # print(git_cmd)



    jit = git.Git(this_workspace)
    loginfo = jit.log(*git_cmd)

    with_stat = form_params.get("stat")
    if with_stat:
        loginfo = format_commit_log(loginfo)
        # front_column_dispaly.append("stat")
        front_column_dispaly.insert(0, 'stat')

    import re
    # 将内容中ｔｉｔｌｅ字段列下的　' 全部改为''，此为yaml的转义，见：https://www.jianshu.com/p/ffb4e813d557
    REPATTERN_TRANS_SINGLE_QUOT = r"(\n)(\s\stitle:\s')(.*)(')"
    loginfo = re.sub(REPATTERN_TRANS_SINGLE_QUOT, lambda m: m.group(1) + m.group(2) + m.group(3).replace("'", "''") + m.group(4),
                     loginfo, 0)
    print(loginfo)

    # https://blog.csdn.net/qq_39985298/article/details/88866094    Loader=yaml.FullLoader
    loginfo = yaml.load(loginfo, Loader=yaml.FullLoader)
    print("============================================================")
    # print(loginfo)
    print("============================================================")
    # print(type(loginfo))
    if not loginfo:
        loginfo = []

    ret['data'] = {}
    ret['data']['headers'] = front_column_dispaly
    ret['data']['contents'] = loginfo
    return ret




    """
    

    # fetch commits
    # use token to fetch or clone https://stackoverflow.com/questions/25409700/using-gitlab-token-to-clone-without-authentication
    # try:
    #     masterDEhash = this_remote.fetch("master")
    #     tagDEhash = this_remote.fetch("tagname")
    #     # this_remote.fetch("--tags")
    # except:
    #     pass
    #     print("The repo does not update, yet!")


    #以下步骤可以和this_remote.fetch() 用协程（async/await）异步同时执行

    # https://stackoverflow.com/questions/41684991/datetime-strptime-2017-01-12t141206-000-0500-y-m-dthms-fz
    # from .common import utc_to_local
    # local_start_date = utc_to_local(request.data["since"], utc_format='%Y-%m-%dT%H:%M:%S.%fZ', local_format="%Y-%m-%d")
    # print(local_start_date)

    # django 获取ｐｏｓｔ来的数据　https://www.jianshu.com/p/860859b7dea5　postman向Django-restframework 提交三种数据类型总结


    # filter commits
    # 参数解析方法见 site-packages/git/cmd.py 中的 transform_kwargs & transform_kwarg 两个方法　*****
    # from git import Git


    jit = git.Git(".")
    loginfo = jit.log(
        "--no-merges", "--color", "--date=format:'%Y-%m-%d %H:%M:%S'", "--author=t",
        # "--stat",
        "--pretty=format:'%Cred%h%Creset -%C(yellow)%d%Cblue %s %Cgreen(%cd) %C(bold blue)<%an>%Creset'",
        "--abbrev-commit",
        "origin/master",
        # "--", "test1.file",
        "--max-count=3",
        "--skip=3",
    )
    print (loginfo)

    # OR 
    # loginfo = jit.log(
    #     "--no-merges", "--color", "--date=format:'%Y-%m-%d %H:%M:%S'", "--author=",
    #     # "--stat",
    #     "--pretty=format:'%h -\n%d %s (%cd) <%an>'",
    #     "--abbrev-commit",
    #     "origin/master",
    #     # "--", "test1.file",
    #     # "--max-count=3",
    #     # "--skip=3",
    # )
    # print(loginfo)

    git config --global alias.lm  "log --no-merges --color --date=format:'%Y-%m-%d %H:%M:%S' --author='你的名字！自己修改！' --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Cblue %s %Cgreen(%cd) %C(bold blue)<%an>%Creset' --abbrev-commit"

    git config --global alias.lms  "log --no-merges --color --stat --date=format:'%Y-%m-%d %H:%M:%S' --author='你的名字！自己修改！' --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Cblue %s %Cgreen(%cd) %C(bold blue)<%an>%Creset' --abbrev-commit"

    git config --global alias.ls "log --no-merges --color --graph --date=format:'%Y-%m-%d %H:%M:%S' --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Cblue %s %Cgreen(%cd) %C(bold blue)<%an>%Creset' --abbrev-commit"

    git config --global alias.lss "log --no-merges --color --stat --graph --date=format:'%Y-%m-%d %H:%M:%S' --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Cblue %s %Cgreen(%cd) %C(bold blue)<%an>%Creset' --abbrev-commit"




    jit = git.Git(".")

    # yaml
    loginfo = jit.log(
        "--no-merges", "--color", "--date=format:'%Y-%m-%d %H:%M:%S'", "--author=",
        # "15fbe62..da5fba7",
        "--stat",
        "--abbrev-commit",
        "origin/master",
        "-G 8",
        # "--", "test1.file",
        "--max-count=5",
        # "--skip=3",

        "--pretty=-\n  shortcommit: %h\n  refnames: '%d'\n  title: %s\n  datetime: %cd\n  author: %an",
    )
    """
