from django.db import models
from django.db.models import Q

# category_choices = ((1, "gitlab"), (2, "jenkins"), (3, "jira"), (4, "saltstack"), (0, "others"))
icon_color_choices = ((1, "red"), (2, "orange"), (3, "yellow"), (4, "olive"), (5, "green"), (6, "teal"), (7, "blue"), (8, "violet"), (9, "purple"), (10, "pink"), (11, "brown"), (12, "grey"), (0, "black"))
project_visible_choices = ((1, "public"), (2, "protected"), (3, "private"), (0, "default"))
prject_member_privilege_choices = ((10, "Guest"), (20, "Reporter"), (30, "Operator"), (40, "Maintainer"), (50, "Owner"))
jenkins_build_status_choices = ((1, "success"), (2, "abort"), (3, "fail"), (4, "building"), (0, "unkown"))

class ProjectManager(models.Manager):
    def get_project(self, req_params, user, visible_level=3, blocked_or_not=False):
        if not req_params:
            return self._get_your_project(user, blocked_or_not)
        if req_params['scop'] == 'yourproject':
            query = self._get_your_project(user, blocked_or_not)
        elif req_params['scop'] == 'starproject':
            query = self._get_starred_project(user, visible_level, blocked_or_not)
        elif req_params['scop'] == 'allproject':
            query = self._get_all_project(user, visible_level, blocked_or_not)
        else:
            query = self.get_queryset().none()
        query = query.filter(name__icontains=req_params['keyword'])
        sort = req_params['sort']
        if sort in ['created_at', '-created_at', 'updated_at', '-updated_at', 'name', '-name', ]:
            return query.order_by(sort)
        else:
            return query

    def _get_your_project(self, user, blocked_or_not):
        query = self.get_queryset().filter(Q(blocked=blocked_or_not) & Q(members__username__contains=user))
        return query

    def _get_starred_project(self, user, visible_level, blocked_or_not):
        query = self.get_queryset().filter(Q(blocked=blocked_or_not) & Q(visible__lt=visible_level) & Q(starers__username__contains=user))
        return query

    def _get_all_project(self, user, visible_level, blocked_or_not):
        query = self.get_queryset().filter(Q(blocked=blocked_or_not) & Q(visible__lt=visible_level) | Q(members__username__contains=user))
        return query