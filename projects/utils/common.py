import pytz, datetime
import collections
from django.conf import settings

import gitlab
import rest_framework
from django.utils.translation import ugettext_lazy as _

def dashify(string):
    return string.replace(' ','')


# 为了防止最后拼接的参数顺序被打乱（ｄｉｃｔ是无序的），ｏｒ使用OrderedDict？
# 最后的　branches　信息就必须在　file　信息之前
filter_commit_params = [
    "branches",  # one_or_many
    # "start_tag",
    # "end_tag",
    "Vscope", # 一个Vscope已经转换成以上的"start_tag"和"end_tag"

    "since",
    "until",

    "author",
    "grep",  # one_or_many this filter commit message --grep
    # "files", # one_or_many  -- filename  加上　-- 　是为了和branch 信息区分。　　这个要放到最后
    "G",
    # one 这个填多个【git log -S"8" -S"9" OR或 git log -G"\s9" -G"\s8"  都只有最后一个-S或-G生效】
    # this filter keyword in any files -S"<string>"、-G"<string>" 另外：如果你想使用正则表达式去匹配而不是字符串, 那么你可以使用-G代替-S. 注：-S后没有"="，与查询内容之间也没有空格符

    "merges_or_no",
    # --no-merges Do not print commits with more than one parent. This is exactly the same as --max-parents=1.

    "stat",

    "limit",  # --max-count=<number> Limit the number of commits to output.
    "offset",  # --skip=<number> Skip number commits before starting to show the commit output.

    "files",  # one_or_many  -- filename  加上　-- 　是为了和branch 信息区分。　　这个要放到最后
    "",  #
]

display_commit_columns = collections.OrderedDict({
    # "col_shortcommit": "\n  shortcommit: %h", # must be choosed
    "refnames": ("\n  refnames: '%d'", "refnames"), #
    "col_title": ("\n  title: '%s'", "title"), #
    "col_datetime": ("\n  datetime: %cd", "datetime"), #
    "col_author": ("\n  author: '%an'", "author"), #
})


#######################################
def hashlize_branchtag_func(param, this_project):
    try:
        short_id = this_project.obtain_project_commit(param).short_id
    except gitlab.exceptions.GitlabGetError as e:
        msg = _(e.error_message + " => " + param)
        raise rest_framework.exceptions.NotFound(msg, code='not found')
    return short_id
# gitlab
    # response_code = result.status_code,
    # error_message = error_message,
    # response_body = result.content
# restframework
    # status_code = status.HTTP_404_NOT_FOUND
    # default_detail = _('Not found.')
    # default_code = 'not_found'

def hashlize_branchtag(form_params, this_project, need_hashlize=["branches", "startPoint", "endPoint"]):
    for value in need_hashlize:
        param = form_params.get(value, None)
        print(param)
        if not param:
            continue
        if isinstance(param, (list, tuple)):
            commit_results = []
            for v in param:
                commit_results.append(hashlize_branchtag_func(v, this_project))
            form_params[value] = commit_results
        else:
            form_params[value] = hashlize_branchtag_func(param, this_project)

def localize_date(form_params, need_localize=["since", "until"]):
    for value in need_localize:
        trance_date = form_params.get(value, None)
        if trance_date:
            form_params[value] = utc_to_local(trance_date, utc_format='%Y-%m-%dT%H:%M:%S.%fZ', local_format="%Y-%m-%d")

def handle_point(form_params):
    start_point = dashify(form_params.pop("startPoint", ""))
    end_point = dashify(form_params.pop("endPoint", ""))
    if start_point and end_point:
        # sp = start_point if not start_point.startswith("B") else "origin/%s"%start_point[1:]
        # ep = end_point if not end_point.startswith("B") else "origin/%s"%end_point[1:]
        # form_params["Vscope"] = "%s..%s"%(sp, ep)
        form_params["Vscope"] = "%s..%s" % (start_point, end_point)
    elif start_point or end_point:
        msg = _("plz fill with start point and end point")
        raise rest_framework.exceptions.ParseError(msg, code='not found')



# https://blog.csdn.net/xiongzaiabc/article/details/81456542 将UTC、EST时区的时间转化成北京时间（python）
# 时间格式2018-08-02T14:17:39+00:00  中，最后的+00:00表示的是UTC时间，如果是+01:00，则需要去找到UTC+01:00对应的时区名称，然后放入pytz.timezone（‘’）中，按住ctrl、点击timezone函数，里面有所有时区对应的名称
def utc_to_local(utc_time_str, utc_format='%Y-%m-%dT%H:%M:%S+00:00', local_format="%Y-%m-%d %H:%M:%S"):
    # local_tz = pytz.timezone('Asia/Chongqing')
    local_tz = pytz.timezone(settings.TIME_ZONE)
    # local_format = "%Y-%m-%d %H:%M:%S"
    utc_dt = datetime.datetime.strptime(utc_time_str, utc_format)
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
    # print(local_dt)
    time_str = local_dt.strftime(local_format)
    return time_str

# EST是美国东部标准时间Eastern Standard Time的缩写
def est_to_local(est_time_str, est_format='%b %d, %Y %I:%M %p EST'):
    est_tz = pytz.timezone('EST') # 标注时间的时区
    # local_tz = pytz.timezone('Asia/Chongqing') # 北京时区
    local_tz = pytz.timezone(settings.TIME_ZONE) # 北京时区
    local_format = "%Y-%m-%d %H:%M:%S"  # 所需要的时间打印格式
    est_dt = datetime.datetime.strptime(est_time_str, est_format)
    local_dt = est_dt.replace(tzinfo=est_tz).astimezone(local_tz) # 将原有时区换成我们需要的
    time_str = local_dt.strftime(local_format)
    # return time_str

#######################################
def transform_display_columns(form_params, display_commit_columns=display_commit_columns):
    # display_parms = "--pretty=-"
    display_parms = "--pretty=-\n  shortcommit: %h"
    front_column_dispaly = ["shortcommit"]
    for k, v in display_commit_columns.items():
        this_param = form_params.get(k)
        if this_param:
            display_parms += v[0]
            front_column_dispaly.append(v[1])
    return (display_parms, front_column_dispaly)

def transform_filter_parme(name, value):
    if len(name) == 1:
        if value is True:
            return ["-%s" % name]
        elif type(value) is not bool:
            # 此处不需要dashify(value)　支持两类正则？　【-G www  -G\swww】
            return ["-%s%s" % (name, value)]
    else:
        if value is True:
            return ["--%s" % name]
        elif type(value) is not bool:
            value = dashify(value)
            # if this key startswith "V" then only left the value
            if name == "branches" or name.startswith("V"):
                return [value]
            elif name == "files":
                # 幸好可以　-- test2.py -- test1.py　ＯＲ　-- test2.py test1.py
                return ["--", value]
            elif name == "merges_or_no":
                if value == "default":
                    return []
                else:
                    return ["--%s"%value]
            else:
                return ["--%s=%s" % (name, value)]
    return []

def transform_filter_parmes(form_params, filter_commit_params=filter_commit_params):
    args = []
    for filter_parme in filter_commit_params:
        this_param = form_params.get(filter_parme)
        if not this_param:
            continue
        if isinstance(this_param, (list, tuple)):
            for value in this_param:
                args += transform_filter_parme(filter_parme, value)
        else:
            args += transform_filter_parme(filter_parme, this_param)
        # if this_param:
        #     if isinstance(this_param, (list, tuple)):
        #         for value in this_param:
        #             args += transform_filter_parme(filter_parme, value)
        #     else:
        #         args += transform_filter_parme(filter_parme, this_param)
    return args

#######################################
def format_commit_log(loginfo):
    import re
    # REPATTERN_FULL = r"^\s(\d+)\D+(\d+)\D+(\d+)\D+\n"
    # https://www.runoob.com/python/python-reg-expressions.html python re
    # https://www.cnblogs.com/tina-python/p/5508402.html        python re

    loginfo = re.sub(r"(\n)(\n)", lambda m: m.group(0) + "  effectfiles:" + m.group(1), loginfo, 0)

    REPATTERN_FULL = r"(\n)(\s)(?P<file>[^\s]+)(\s+\|\s+)(?P<count>\d+)(.*)"
    # loginfo = re.sub(REPATTERN_FULL, '\n\s\s\s', loginfo)  不应该用\s，而应该直接空格
    loginfo = re.sub(REPATTERN_FULL, lambda m: m.group(1) + m.group(2) * 4 + m.group('file') + ": " + m.group('count'),
                     loginfo, 0)

    REPATTERN_BOTH = r"(\n)(\s)(?P<changecnt>\d+)(\s+files?\s+changed,\s+)(?P<insertcnt>\d+)(\s+insertions?)(.*)(?P<deletecnt>\d+)(\s+deletions?)(.*)"
    loginfo = re.sub(REPATTERN_BOTH, lambda m: "\n  stats:" + m.group(1) + m.group(2) * 4 + "changecnt: " + m.group(
        "changecnt") + "\n    insertcnt: " + m.group("insertcnt") + "\n    deletecnt: " + m.group("deletecnt"), loginfo,
                     0)
    REPATTERN_INS = r"(\n)(\s)(?P<changecnt>\d+)(\s+files?\s+changed,\s+)(?P<insertcnt>\d+)(\s+insertions?)(.*)"
    loginfo = re.sub(REPATTERN_INS, lambda m: "\n  stats:" + m.group(1) + m.group(2) * 4 + "changecnt: " + m.group(
        "changecnt") + "\n    insertcnt: " + m.group("insertcnt"), loginfo, 0)
    REPATTERN_DEL = r"(\n)(\s)(?P<changecnt>\d+)(\s+files?\s+changed,\s+)(?P<deletecnt>\d+)(\s+deletions?)(.*)"
    loginfo = re.sub(REPATTERN_DEL, lambda m: "\n  stats:" + m.group(1) + m.group(2) * 4 + "changecnt: " + m.group(
        "changecnt") + "\n    deletecnt: " + m.group("deletecnt"), loginfo, 0)
    # print("result:")
    # print(loginfo)
    return loginfo

#######################################
import os, git
from urllib.parse import urlparse, urljoin
def makeDir(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    # os.chdir(dirname)

def getRepoAndUpdateRemoteOrigin(this_workspace, gl, gitlab_token, origin_forth):
    try:
        repo = git.Repo(this_workspace)
        print("yes")
    except:
        repo = git.Repo.init(this_workspace)
        print("no")

    try:
        current_user = gl.get_current_user()
        current_user_id = current_user.id
        gl.get_project_member(origin_forth, current_user_id)

        projectinfo = gl.get_project(origin_forth)
        http_url_to_repo = projectinfo.http_url_to_repo
        # https://www.cnblogs.com/itlqs/p/6055365.html
        parsed_url = urlparse(http_url_to_repo)
        # print(parsed_url)
        authed_http_url_to_repo = urljoin("%s://oauth2:%s@%s" % (parsed_url.scheme, gitlab_token, parsed_url.netloc), parsed_url.path)
        # 更新远程仓库中的　remote origin中的认证账号
        if "origin" in [i.name for i in repo.remotes]:
            repo.delete_remote('origin')
        repo.create_remote('origin', authed_http_url_to_repo)
        return repo
    except:
        # ret['code'] = 1001
        # ret['msg'] = "Current user does not in this project."
        msg = _("当前设置的账户没有该仓库的代码权限，plz check!")
        raise rest_framework.exceptions.PermissionDenied(msg, code='not found')
        # raise rest_framework.exceptions.PermissionDenied()
