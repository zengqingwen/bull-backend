import gitlab


# # 登录
# gl = gitlab.Gitlab(url, token)

class GitlabAPI():
    def __init__(self, url, token):
        self.gl = gitlab.Gitlab(url, token)

    def get_projects(self, page):
        return self.gl.projects.list(page=page)

    def search_projects(self, keyword):
        return self.gl.projects.list(search=keyword, per_page=100)

    def get_project(self, project_id_or_project_name):
        return self.gl.projects.get(project_id_or_project_name)
        # try:
        #     projectinfo = self.gl.projects.get(project_id_or_project_name)
        # except Exception as e:
        #     projectinfo = {"result": "project %s not exist or you do not have privilege." % project_id_or_project_name}
        #     print(e)
        # return projectinfo

    def get_project_info(self, path_with_namespace):
        for project in self.get_projects():
            if project.path_with_namespace == path_with_namespace:
                return project.attributes
        return "This project is not exist, plz check."

    def get_project_branches(self, project_id_or_project_name):
        return self.get_project(project_id_or_project_name).branches.list(all=True)

    def get_project_tags(self, project_id_or_project_name):
        return self.get_project(project_id_or_project_name).tags.list(all=True)

    def get_project_commit(self, project_id_or_project_name, commit_hash):
        return self.get_project(project_id_or_project_name).commits.get(commit_hash)

    def get_project_branch(self, project_id_or_project_name, branch_name):
        return self.get_project(project_id_or_project_name).branches.get(branch_name)

    def get_project_tag(self, project_id_or_project_name, tag_name):
        return self.get_project(project_id_or_project_name).tags.get(tag_name)

    def get_project_users(self, project_id_or_project_name):
        return self.get_project(project_id_or_project_name).users.list()

    def get_project_members(self, project_id_or_project_name):
        return self.get_project(project_id_or_project_name).members.list()

    def get_current_user(self):
        self.gl.auth()
        return self.gl.user

    def get_users(self):
        return self.gl.users.list(all=True)

    def get_user(self, user_id):
        try:
            userinfo = self.gl.users.get(user_id)
        except Exception as e:
            userinfo = {"result": "user %s not exist or you do not have privilege." % user_id}
            print(e)
        return userinfo

    def get_user_by_username(self, user_name):
        return self.gl.users.list(username=user_name)

    def get_groups(self):
        return self.gl.groups.list(all=True)

    def get_group(self, group_id_or_group_name):
        try:
            groupinfo = self.gl.groups.get(group_id_or_group_name)
        except Exception as e:
            groupinfo = {"result": "group %s not exist or you do not have privilege." % group_id_or_group_name}
            print(e)
        return groupinfo

    # priority
    def get_project_member(self, project_id_or_project_name, user_id):
        # return self.get_project(project_id_or_project_name).users.get(user_id)
        return self.get_project(project_id_or_project_name).members.get(user_id)

    ### *****省去多次声明
    def obtain_project(self, project_id_or_project_name):
        try:
            self.projectinfo = self.gl.projects.get(project_id_or_project_name)
        except Exception as e:
            projectinfo = {"result": "project %s not exist or you do not have privilege." % project_id_or_project_name}
            print(e)
        return self

    def obtain_project_commit(self, commit_hash):
        return self.projectinfo.commits.get(commit_hash)



# http://192.168.56.71/help/api/users.md
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/users.html

ggg = GitlabAPI("http://192.168.56.71", "dYZdJmjqJgp8uyqcTFsF")
# print(ggg.get_project("5").default_branch)
# print(ggg.get_project_branches(5)[1])
# print(ggg.get_project_tags(5))
# print(ggg.get_project_commit(5, "2c1d41c7"))
# print(ggg.get_project_commit(5, "1"))
# print(ggg.get_project_branch(5, "dev"))
# print(ggg.get_project_tag(5, "2"))

# try:
#     print(ggg.obtain_project(5).obtain_project_commit("2c1d41c78"))
# except gitlab.exceptions.GitlabGetError:
#     raise gitlab.exceptions.GitlabGetError("wobu")
# print("finallly")

# print(ggg.obtain_project(5).projectinfo.commits.get("2c1d41c7"))
# print(ggg.obtain_project("repo_test/test1").projectinfo.commits.get("2c1d41c7"))

# print(ggg.search_projects("jingdong"))
# print(ggg.get_project("jingdong/todo"))
# print(ggg.get_project("repo_test/test1").users.list())
# print(ggg.get_users())
# print(ggg.get_user_by_username("root"))
# print(ggg.get_project_member("repo_test/test1", "2"))

# get_current_user
# aaa = gitlab.Gitlab("http://192.168.56.71", "dYZdJmjqJgp8uyqcTFsF")
# aaa.auth()
# print(aaa)
# print(aaa.user)
# print(ggg.get_current_user().id)
# http_url_to_repo = ggg.get_project("repo_test/test1").http_url_to_repo
# print(http_url_to_repo)
# from urllib.parse import urlparse
# from urllib.parse import urljoin
# o = urlparse(http_url_to_repo)
# print(o)
# a = urljoin("%s://oauth2:%s"%(o.scheme, o.netloc), o.path)
# print(a)
# print(ggg.get_project_member("5", "2"))