# from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

from .utils.models_extention import *
# Create your models here.


class Category(models.Model):
    """
    project的种类
    """
    # type = models.SmallIntegerField(choices=category_choices, default=0, verbose_name="工程种类")
    type = models.CharField(max_length=20, unique=True, verbose_name="工程种类")
    icon_color = models.SmallIntegerField(choices=icon_color_choices, default=0, verbose_name="工程种类图标颜色")  # 工程种类图标颜色
    remark = models.CharField(max_length=100, null=True, blank=True, verbose_name="备注信息")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    def __str__(self):
        return self.type # 此object显示的字段

class Credential(models.Model):
    """
    认证对接项目
    """
    name = models.CharField(max_length=30, verbose_name="credential名称") # 给Credential起的名称
    username = models.CharField(max_length=30, verbose_name="账号") # 对接项目的账号
    token = models.CharField(max_length=128, verbose_name="token") # 对接项目的token

    creator = models.ForeignKey(User, related_name='cre_user', on_delete=models.SET_NULL, null=True, blank=True) # 删除级联
    category = models.ForeignKey(Category, related_name='cre_category', on_delete=models.CASCADE) # 删除级联

    remark = models.CharField(max_length=100, null=True, blank=True, verbose_name="备注信息")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        unique_together = ('creator', 'name',)

    def __str__(self):
        return f"{self.name}-{self.creator}-{self.category}"  #

class Project(models.Model):
    """
    工程
    """
    basic_regex = RegexValidator(regex=r'(^[a-z|A-Z|0-9|_|-]+$)',
                                  message="This field must be entered in the format: 'a-z|A-Z|0-9|_|-'. Up to 30 digits allowed.")
    name = models.CharField(validators=[basic_regex], max_length=30, verbose_name="工程名") # 对应bull的工程名
    # 这些origin_参数虽然在Model里设置的是null=True, blank=True，但是真正在serializers中判断还是不能为空的
    # ①
    origin_url = models.URLField(verbose_name="对接项目地址", null=True, blank=True) # 对接项目的地址
    #################################################################################################################################
    # refer to **** https://stackoverflow.com/questions/19130942/whats-the-best-way-to-store-phone-number-in-django-models
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
    #                              message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    # phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)  # validators should be a list
    # https://www.codingforentrepreneurs.com/blog/the-simple-power-of-django-validators/   现成的　from django.core.validators import validate_slug, validate_email
    # https://blog.csdn.net/suzimuyu99/article/details/80924402  Python正则\w匹配中文的问题
    # \w：匹配任意字母数字及下划线，等价于[a-zA-Z0-9_]            \W：匹配任意非字母数字及下划线，等价于[^\w]
    #################################################################################################################################
    normal_regex = RegexValidator(regex=r'(^[\w|.|-]+$)',
                                  message="This field must be entered in the format: '汉字|a-z|A-Z|0-9|.|-'. Up to 30 digits allowed.")
    origin_jack_name = models.CharField(validators=[normal_regex], max_length=30, verbose_name="对接项目名", null=True, blank=True)  # 对接项目的项目名
    origin_third = models.CharField(validators=[normal_regex], max_length=30, verbose_name="对接项目的第三个扩展属性", null=True, blank=True)  # 对接项目的第三个扩展属性
    origin_forth = models.CharField(validators=[normal_regex], max_length=30, verbose_name="对接项目的第四个扩展属性", null=True, blank=True)  # 对接项目的第四个扩展属性
    # home_dir = models.FilePathField() # 该工程的家目录
    blocked = models.BooleanField(default=False)
    visible = models.SmallIntegerField(choices=project_visible_choices, default=1, verbose_name="可见范围")

    creator = models.ForeignKey(User, related_name='pro_owner', on_delete=models.SET_NULL, null=True, blank=True)  # 删除级联，用于访问url中的<pk> namespace
    # null=True, blank=True，说明Project　不一定要认证通过才能使用
    category = models.ForeignKey(Category, related_name='pro_category', on_delete=models.SET_NULL, null=True, blank=True)  # 删除置空
    credential = models.ForeignKey(Credential, related_name='pro_credential', on_delete=models.SET_NULL, null=True, blank=True)  # 删除置空
    members = models.ManyToManyField(User, related_name='pro_member', verbose_name="成员", through='ProjectMember') # 工程的成员，与用户多对多，借此实现去权限控制
    # https://github.com/dokterbob/django-portfolio/issues/5   Warning on runserver, "null has no effect on ManyToManyField"
    # 因为多对多本身就自带null=True
    starers = models.ManyToManyField(User, related_name='pro_starer', blank=True, verbose_name="关注者")

    # 自关联　通过django自带的自关联功能，实现串级工程
    # eg: (steve_jira(产生需求) => steve_gitlab(代码实现) => steve_jekins(编译验证))，这样一个串行的需求走向（有关联性的）
    # next_stage_project = models.ForeignKey('self', null=True, blank=True, verbose_name="下级项目")

    remark = models.CharField(max_length=100, null=True, blank=True, verbose_name="备注信息")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        ordering = ['-created_at']
        unique_together = ('creator', 'name',)
        verbose_name = '工程'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    objects = ProjectManager()

# refer to: https://blog.csdn.net/hpu_yly_bj/article/details/78941104
class ProjectMember(models.Model):
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    # 添加额外字段
    privilege = models.SmallIntegerField(choices=prject_member_privilege_choices, default=10, verbose_name="工程权限")

    class Meta:
        unique_together = ('member', 'project',)
        db_table = 'project_member_privilege_relationship'
        verbose_name = '工程下用户的权限'
        verbose_name_plural = verbose_name

class ProjectJenkins(models.Model):
    """
    Jenkins类工程特有的一些状态/参数
    """
    last_success_build = models.IntegerField(null=True, blank=True)
    last_abort_build = models.IntegerField(null=True, blank=True)
    last_faild_build = models.IntegerField(null=True, blank=True)
    last_success_build_duration = models.IntegerField(default=24 * 60 * 60) # 最近一次成功的构建持续了多少时间(默认初始值为24小时)，借此判断下次构建的进度条

    porject = models.OneToOneField(Project, on_delete=models.CASCADE, related_name="pro_jen_project")

    class Meta:
        verbose_name = '工程-jenkins'
        verbose_name_plural = verbose_name


# 不同的project生成的结果，记录在不同的数据表里，这样尽量减少产生一个超大表
# 一次性触发，触发结束（成功/失败），把相关信息记录到如下表中
class HistoryGitlab(models.Model):
    """
    gitlab类工程特有的一些执行结果/记录
    """
    exe_parms = models.CharField(max_length=300, verbose_name="执行参数") # 记录每一次的执行操作，保存为json字串

    # porject = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="his_gitlab_project")
    trigger = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="his_gitlab_user")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        verbose_name = '执行记录-gitlab'
        verbose_name_plural = verbose_name

# 第一次触发，触发结束（成功/失败），把相关信息（exe_parms，origin_build_no，status(等待中/执行中)，build_start_time，trigger）记录到如下表中，接着触发异步请求
# 后续异步(celery)请求，（如果status未结束），继续请求，不写入数据库；（如果status结束(成功/失败/取消等)）把相关信息（build_end_time，build_duration，build_archives，status(成功/失败/取消等)）更新到如下表中
# (celery)正常请求结束或者超时结束后，还要更新class ProjectJenkins(models.Model):　这个表
# 三类状态切换点才更新数据库（等待中　＝》　执行中　＝》　成功/失败/取消等）
class HistoryJenkins(models.Model):
    """
    Jenkins类工程特有的一些执行结果/记录
    """
    exe_parms = models.CharField(max_length=300, verbose_name="执行参数")  # 记录每一次的执行操作，保存为json字串
    origin_build_no = models.IntegerField(null=True, blank=True)
    status = models.SmallIntegerField(choices=jenkins_build_status_choices)
    build_start_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    build_end_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    build_duration = models.IntegerField(null=True, blank=True) # 构建持续时间
    build_archives = models.URLField() # 构建产物的下载链接

    # porject = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="his_jenkins_project")
    trigger = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="his_jenkins_user")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        verbose_name = '执行记录-jenkins'
        verbose_name_plural = verbose_name

    def __str__(self):
        return f"{self.origin_build_no}-{self.get_status_display()}"  #