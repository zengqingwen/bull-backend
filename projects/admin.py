from django.contrib import admin
from .models import *

# Register your models here.

class CredentialInline(admin.TabularInline):
    model = Credential
class CategoryAdmin(admin.ModelAdmin):
    inlines = [CredentialInline]
    list_display = ('type', 'created_at', 'updated_at',)
    search_fields = ('type',)
    list_filter = ('type',)


class CredentialAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'updated_at',)
    fieldsets = (
        ['Main', {
            'fields': ('name', 'creator', 'category',),
        }],
        ['Advance', {
            'classes': ('collapse',),
            'fields': ('username', 'token', 'remark',),
        }]
    )
    search_fields = ('type',)

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'blocked', 'credential',)
    search_fields = ('name',)
    list_filter = ('blocked',)

class ProjectJenkinsAdmin(admin.ModelAdmin):
    list_display = ('porject',)
    search_fields = ('porject',)
    list_filter = ('porject',)
#

admin.site.register(Category, CategoryAdmin)
admin.site.register(Credential, CredentialAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectJenkins, ProjectJenkinsAdmin)

admin.site.register([ProjectMember, HistoryGitlab, HistoryJenkins, ])