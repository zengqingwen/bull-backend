# viewsets
from rest_framework import viewsets
from rest_framework.mixins import ListModelMixin

# apiview
from rest_framework import generics
# from rest_framework.generics import GenericAPIView
from .extendMixins import MultipleFieldLookupMixin
from rest_framework.response import Response

from ..models import *
from .serializers import (
    CategorySerializer,
    CredentialSerializer,
    ProjectSerializer
)
from rest_framework.authtoken.models import Token

from django.db.models import Q

class CategoryViewSet(ListModelMixin, viewsets.GenericViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

# class CredentialCreateAPIView(generics.CreateAPIView):
class CredentialViewSet(viewsets.ModelViewSet):
    queryset = Credential.objects.all()
    serializer_class = CredentialSerializer

    # maybe this use models.Manager ???
    def get_queryset(self):
        user = self.request.user # or user = self.request._request.user
        if user:
            return Credential.objects.filter(creator=user)
        return Credential.objects.none()
        # token = self.request._request.META.get('HTTP_AUTHORIZATION') # or token = self.request.META.get('HTTP_AUTHORIZATION')
        # token = token.split()[1]
        # token_obj = Token.objects.filter(key=token).first()
        # if token_obj:
        #     user = token_obj.user
        #     return Credential.objects.filter(creator=user)
        # return Credential.objects.none()

class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_queryset(self):
        user = self.request.user
        print(user)
        # refer to: https://www.cnblogs.com/linjiqin/p/3817814.html
        req_params = self.request.query_params.dict()
        # print(req_params)
        return Project.objects.get_project(req_params, user)

        # scop = Project.objects._get_all_project(user)
        # return scop.filter(name__icontains=req_params['keywor']).order_by(req_params['sort'])

        # return Project.objects.filter(Q(blocked=False) & Q(visible__lt=3), Q(members__username__contains=user))
        # return Project.objects.filter(Q(blocked=False) & Q(visible__lt=3) | Q(members__username__contains=user))


        # print(Project.objects.get(name='1111').members.all())
        # print(type(Project.objects.get(name='1111').members.all()))

        # p1 = Project.objects.filter(name='1111').last()
        # # many to many 查
        # print(ProjectMember.objects.filter(project=p1, member=user).last())
        # print(ProjectMember.objects.filter(project=p1))

        # # many to many 增
        # https://blog.csdn.net/u010377372/article/details/80699439
        # try:
        #     m1 = ProjectMember(project=p1, member=user)
        #     m1.save()
        #     # m2 = ProjectMember.objects.create(project=p1, member=user)    #　OR
        # except Exception as e:
        #     print("This member already in this project.")
        #################################################################################
        #         or like below :                                                       #
        #         refer to: https://blog.csdn.net/u010377372/article/details/80699439   #
        #         def perform_create(self, serializer):                                 #
        #             queryset = BusinessCard.objects.filter(user=self.request.wxuser)  #
        #             if queryset.exists():                                             #
        #                 raise ValidationError('名片已存在')                             #
        #             instance = serializer.save(user=self.request.wxuser)              #
        #             return instance                                                   #
        #################################################################################

        # # many to many 删（符合条件的全删）
        # ProjectMember.objects.filter(project=p1, member=user).delete()

        # # 改
        # ProjectMember.objects.filter(project=p1, member=user).last().update(privilege=30) # 这种方法不行
        # pm1 = ProjectMember.objects.filter(project=p1, member=user).last()
        # pm1.privilege = 40
        # pm1.save()
        # return Project.objects.filter(Q(blocked=False) | Q(visible__lt=3) | Q(members__username__contains=user))


# now you can ProjectDetail via /projects/(?P<pk>\d+)/ (the previous way)
#                           and /retriveprojectbyname/<slug:creator__username>/<slug:name>/ (this way)
# refer to belows:
# ***** http://www.itdaan.com/blog/2016/07/19/6e1904750dbeeca676273da2b8b7cdd3.html django rest框架的多个lookup_fields
#     The official docs have an example for this at https://www.django-rest-framework.org/api-guide/generic-views/#creating-custom-mixins
#      https://www.cnblogs.com/haoshine/p/5395540.html      django get_object_or_404
# http://www.itdaan.com/blog/2017/08/07/6f571a4492a1e39f3f34448993ad3ae.html 即访问多/<slug:creator__username>/<slug:name>/，也包含生成多/<slug:creator__username>/<slug:name>/
######################################################################################################
# https://blog.csdn.net/fanlei5458/article/details/80638348     django 使用 request 获取浏览器发送的参数
# https://www.jianshu.com/p/4b746e566d78    django rest framework 获取前端参数的几种方式 【request.data.getlist('username')】
# https://www.jianshu.com/p/f2f73c426623    django-rest-framework解析请求参数
class ProjectDetailAPIView(MultipleFieldLookupMixin, generics.RetrieveAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    lookup_fields = ('creator__username', 'name')
