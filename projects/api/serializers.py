from rest_framework import serializers
from rest_framework import fields
from django.utils.translation import ugettext_lazy as _
from ..models import *
from rest_framework.validators import UniqueTogetherValidator

from django.db import transaction

from django.contrib.humanize.templatetags.humanize import naturaltime

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "type")

class CredentialSerializer(serializers.ModelSerializer):
    # creator = serializers.HiddenField(default=serializers.CurrentUserDefault())
    # refer to: https://stackoverflow.com/questions/51940976/django-rest-framework-currentuserdefault-with-serializer
    # owner_id = serializers.HiddenField(default=serializers.CurrentUserDefault())
    # creator = serializers.CharField(source='creator.username', read_only=True)

    # refer to: https://www.django-rest-framework.org/api-guide/serializers/
    # for POST,　仅为校验之用(validate or not)，【creator提供了default值，所以可以不用post；　category需要post来的是个字符串类型的】
    # for GET this is a User Model object; for POST this is a read_only but have already offered a default value.
    creator = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())
    # GET, POST 都对应的是 Category Model中的def __str__(self): 返回的字段
    category = serializers.CharField(required=True, help_text=_('Required. the category field'))

    # extral outputs, for GET
    cate = serializers.CharField(source='category.type', read_only=True)
    color = serializers.CharField(source='category.get_icon_color_display', read_only=True)
    class Meta:
        model = Credential
        fields = ("__all__")
        # fields = ("id", "name", "username", "token", "creator", "category", 'remark')
        read_only_fields = ('id', 'created_at', 'updated_at')
        validators = [
            UniqueTogetherValidator(
                queryset=Credential.objects.all(),
                fields=('creator', 'name'),
                message='Credential name already exsit!'
            )]

    @transaction.atomic
    def create(self, validated_data):
        name = validated_data['name']
        username = validated_data['username']
        token = validated_data['token']
        category = validated_data['category']
        remark = validated_data.get('remark', '')

        # Get category object
        try:
            category = Category.objects.get(type=category)
        except Category.DoesNotExist:
            raise serializers.ValidationError('Category does not exist, please enter correct category type')


        # creator = self.context['request'].user
        # print(creator)
        # print(fields.CurrentUserDefault())

        # # Get the requesting user
        # user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            creator = request.user
        else:
            raise serializers.ValidationError('Must be authenticated to create thread')

        # Create the credential
        credential = Credential(
            name=name,
            username=username,
            token=token,
            creator=creator,
            category=category,
            remark=remark
        )
        credential.save()
        return credential

    # def update(self, instance, validated_data): # put
    @transaction.atomic
    def update(self, instance, validated_data, partial=True): # patch
        # Update fields if there is any change
        category = validated_data.pop("category", None)
        # Get category object
        if category:
            try:
                category = Category.objects.get(type=category)
            except Category.DoesNotExist:
                raise serializers.ValidationError('Category does not exist, please enter correct category type')
            # setattr(instance, category, category)
            validated_data['category'] = category

        for field, value in validated_data.items():
            if value != '':
                setattr(instance, field, value)
        instance.save()
        return instance
        # super(CredentialSerializer, self).update(instance, validated_data)

class ProjectSerializer(serializers.ModelSerializer):
    # creator = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())
    creator = serializers.PrimaryKeyRelatedField(write_only=True, queryset=User.objects.all(), default=serializers.CurrentUserDefault())
    # GET, POST 都对应的是 Category Model中的def __str__(self): 返回的字段
    category = serializers.CharField(required=True, help_text=_('Required. the category field'), write_only=True)
    credential = serializers.CharField(required=False, allow_blank=True, help_text=_('Required. the credential field'))

    # extral outputs, for GET
    cate_type = serializers.CharField(source='category.type', read_only=True)
    cate_icon_color = serializers.CharField(source='category.get_icon_color_display', read_only=True)
    credential_id = serializers.IntegerField(source='credential.id', read_only=True)
    members_count = serializers.SerializerMethodField(read_only=True)
    create_time_natural = serializers.SerializerMethodField(read_only=True)
    owner = serializers.CharField(source='creator.username', read_only=True)

    class Meta:
        model = Project
        fields = ("__all__")
        # fields = ("id", "name", "origin_url", "origin_jack_name", "home_dir", "category", 'remark')
        read_only_fields = ('id', 'blocked', 'created_at', 'updated_at')
        validators = [
            UniqueTogetherValidator(
                queryset=Project.objects.all(),
                fields=('creator', 'name'),
                message='Project name already exsit!'
            )]

    def get_members_count(self, obj):
        return ProjectMember.objects.filter(project=obj).count()

    def get_create_time_natural(self, obj):
        return naturaltime(obj.created_at)

    @transaction.atomic
    def create(self, validated_data):
        name = validated_data['name']
        # home_dir = validated_data['home_dir']
        visible = validated_data['visible']
        category = validated_data['category']
        credential = validated_data.get('credential', "")
        remark = validated_data.get('remark', '')
        print(credential)
        print("$$$$$$")

        # Get category object
        try:
            category_object = Category.objects.get(type=category)
        except Category.DoesNotExist:
            raise serializers.ValidationError('Category does not exist, please enter correct category type')

        # # Get the requesting user
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            creator = request.user
        else:
            raise serializers.ValidationError('Must be authenticated to create project')


        # Get credential object
        if credential:
            try:
                credential = Credential.objects.get(name=credential)
            except Credential.DoesNotExist:
                raise serializers.ValidationError('Credential does not exist, please enter correct credential type')

        project_parms = {
            "name": name,
            "visible": visible,
            "creator": creator,
            "category": category_object,
            "remark": remark,
            "credential": credential
        }

        # 判断不同origin_对应的parms
        # ①
        category_base = category.split('-')[0]
        if category_base == 'gitlab':
            category_parmes = ["origin_url", "origin_third", "origin_jack_name"]
        elif category_base == 'jenkins':
            category_parmes = ["origin_url", "origin_jack_name"]
        else:
            category_parmes = ["origin_url"]

        for parme in category_parmes:
            try:
                this_parme = validated_data[parme]
            except:
                raise serializers.ValidationError('This parme %s does not make sence'%parme)
            if this_parme:
                project_parms[parme] = this_parme
            else:
                raise serializers.ValidationError('This parme %s can not be empty'%parme)

        if category_base == 'gitlab':
            gitlab_token = credential.token
            from ..utils.gitlab_handler import GitlabAPI
            gl = GitlabAPI(validated_data["origin_url"], gitlab_token)
            path_with_namespace = validated_data["origin_third"] + "/" + validated_data["origin_jack_name"]
            this_project = gl.get_project(path_with_namespace)
            print(this_project)
            try:
                this_parme_id = this_project.id
            except:
                raise serializers.ValidationError('This gitlab server does not contain this %s project.'%path_with_namespace)
            project_parms["origin_forth"] = this_parme_id

        # category_base = category.split('-')[0]
        # if category_base == 'gitlab':
        #     if not origin_url or not origin_jack_name or not origin_third:
        #         raise serializers.ValidationError('Gitlab_url or gitlab_namespace or gitlab_name can not be empty')
        #     project_parms["origin_url"] = origin_url
        #     project_parms["origin_jack_name"] = origin_jack_name
        #     project_parms["origin_third"] = origin_third
        # elif category_base == 'jenkins':
        #     if not origin_url or not origin_jack_name:
        #         raise serializers.ValidationError('Jenkins_url or jenkins_name can not be empty')
        #     project_parms["origin_url"] = origin_url
        #     project_parms["origin_jack_name"] = origin_jack_name
        # else:
        #     if not origin_url:
        #         raise serializers.ValidationError('Origin_url can not be empty')
        #     project_parms["origin_url"] = origin_url

        # Create the project
        project = Project.objects.create(**project_parms)
        try:
            ProjectMember.objects.create(project=project, member=creator, privilege=50)
        except Exception as e:
            print("Set project owner failed")
        return project
