from django.conf.urls import url
from django.urls import include, path
from django.contrib import admin

from rest_framework.routers import DefaultRouter

from .views import (
    CategoryViewSet,
    CredentialViewSet,
    ProjectViewSet,
    ProjectDetailAPIView,
)

router = DefaultRouter()
router.register(r'credentials', CredentialViewSet)
router.register(r'projects', ProjectViewSet)

urlpatterns = [
    path('categories/', CategoryViewSet.as_view({'get': 'list'}), name='category-list'),
    path(r'', include(router.urls)),
    path('retriveprojectbyname/<slug:creator__username>/<slug:name>/', ProjectDetailAPIView.as_view(), name='project-detail'),
    # path('<int:pk>/', views.DetailTodo.as_view()),
    # 以下这个写法是错的适合url()； path()的要按照上面的写法。
    # path('retriveprojectbyname/(?P<creator__username>[-\w]+)/(?P<name>[-\w]+)/', ProjectDetailAPIView.as_view(), name='project-detail'),

    # path('credential/', CredentialCreateAPIView.as_view({'get': 'list', 'post': 'create'}), name='credential-create'),
    # path('register/', UserCreateAPIView.as_view(), name='user-register'),
    # path('login/', UserLoginAPIView.as_view(), name='user-login'),
    # path('logout/', UserLogoutAPIView.as_view(), name='user-logout'),
    # path('<slug:username>/', UserDetailAPIView.as_view(), name='user-detail'),
    # path('<slug:username>/edit/', UserUpdateAPIView.as_view(), name='user-update'),
    # path('<slug:username>/delete/', UserDeleteAPIView.as_view(), name='user-delete'),
]
