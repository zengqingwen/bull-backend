#!/bin/sh


python manage.py makemigrations
python manage.py migrate
python manage.py loaddata db.json

#python manage.py celery beat &
#python manage.py celery worker &

#特别注意：命令 python manage.py runserver 0:8000 默认监听为本地 127.0.0.1:8000，所以即使通过端口映射出去，外面也无法访问，必须特别指定监听端口为python manage.py runserver 0.0.0.0:8000，监听所有的地址请求，
#还有就是不要把服务都放在后台运行，如果都放在后台，只使用-d参数的话，docker把命令跑完就会自行退出了，所以我的start.sh如下:
python manage.py runserver 0:8000 #前台运行
#注意： 这样日志结构会比较乱，你也可以拆成3个容器来跑