# use base python image with python 2.7
FROM python:3.6.7

# install mysql-client
#RUN apt-get update
#RUN apt-get install -y mysql-client

# env
#ENV RUN_MODE=DEPLOY

# add project to the image
ADD ./ /app/

# set working directory to /app/
WORKDIR /app/

# install python dependencies
#RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
RUN pip install -r requirement.txt \
        && mkdir db

#EXPOSE 8000

# RUN server after docker is up
#CMD bash ./start.sh
